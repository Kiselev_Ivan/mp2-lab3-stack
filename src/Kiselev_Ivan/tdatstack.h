#ifndef __TDATSTACK__
#define __TDATSTACK__

#include "tdataroot.h"
#include <iostream>
using namespace std;

template<class MTy>
class Stack : public TDataRoot<MTy>
{
private:
	int top;	

public:
	Stack(int Size = DefMemSize) : TDataRoot<MTy>(Size) { top = -1; };
	void PutElem(const MTy &v);	// �������� ��������
	MTy GetElem();				// ������� ��������
	MTy LastElem();				// �������� �������� ���������� ��������
	int IsValid();					// ������������ ���������
	void Print();					// ������ ��������
	int GetTop();				//���������� �������� top
};

template <class MTy>
int Stack<MTy>::GetTop()
{
	return top;
}

template<class MTy>
void Stack<MTy>::PutElem(const MTy &v)
{ // �������� �������� 
	if (!IsValid())
		SetRetCode(DataNoMem);
	else 
		if (IsFull()) 
		{
			void* p = nullptr;
			SetMem(p, MemSize + DefMemSize);
			pMem[++top] = v;
			DataCount++;
		}
	else 
	{
		pMem[++top] = v;
		DataCount++;
	}
}

template<class MTy>
MTy Stack<MTy>::GetElem()
{ // ������� ��������
	if (!IsValid())
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[top--];
	}
	return -1;
}

template<class MTy>
MTy Stack<MTy>::LastElem()
{ // �������� �������� ���������� ��������
	if (!IsValid())
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else {
		return pMem[top];
	}
	return -1;
}

template<class MTy>
int Stack<MTy>::IsValid()
{ // ������������ ���������
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0)
		return 0;
	return 1;
}

template<class MTy>
void Stack<MTy>::Print()
{ // ������ ��������
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}

#endif