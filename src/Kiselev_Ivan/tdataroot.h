// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem
#include <iostream>

using namespace std;

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

#define brace_1 '('	//открывающая скобка
#define brace_2 ')'	//закрывающая скобка
#define plus	'+'		//плюс
#define minus	'-'		//минус
#define mult	'*'		//умножить		
#define divide	'/'		//поделить

enum TMemType { MEM_HOLDER, MEM_RENTER };

template <class MTy>
class TDataRoot : public TDataCom
{
protected:
	MTy* pMem;      // память для СД
	int MemSize;      // размер памяти для СД
	int DataCount;    // количество элементов в СД
	TMemType MemType; // режим управления памятью

	void SetMem(void *p, int Size);             // задание памяти
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty() const;           // контроль пустоты СД
	virtual bool IsFull() const;           // контроль переполнения СД
	virtual void  PutElem(const MTy &Val)=0; // добавить значение
	virtual MTy GetElem()=0; // извлечь значение

								 // служебные методы
	virtual int  IsValid()=0;                 // тестирование структуры
	virtual void Print()=0;                 // печать значений

											  // дружественные классы
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

template <class MTy>
TDataRoot<MTy>::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else 
	{
		MemSize = Size;
		DataCount = 0;
		if (Size == 0) 
		{
			MemType = MEM_RENTER;
			pMem = nullptr;
		}
		else 
		{
			MemType = MEM_HOLDER;
			pMem = new MTy[MemSize];
		}
	}
}

template <class MTy>
TDataRoot<MTy>::~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = nullptr;
}

template <class MTy>
void TDataRoot<MTy>::SetMem(void *p, int Size)
{
	if (MemType == MEM_RENTER) 
	{
		MemSize = Size;
		for (int i = 0; i < DataCount; ++i)
			((MTy*)p)[i] = pMem[i];
		pMem = (MTy*)p;
	}
	if (MemType == MEM_HOLDER)
	{
		if (Size < 0)
			throw SetRetCode(DataNoMem);
		else 
		{
			MTy *tmp = pMem;
			pMem = new MTy[Size];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = tmp[i];
			MemSize = Size;
			delete[]tmp;
		}
	}
}

template <class MTy>
bool TDataRoot<MTy>::IsEmpty(void) const {
	return DataCount == 0;
}

template <class MTy>
bool TDataRoot<MTy>::IsFull(void) const {
	return DataCount == MemSize;
}



#endif